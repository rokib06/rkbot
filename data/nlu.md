## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there

## intent:goodbye
- bye
- goodbye
- see you around
- see you later

## intent:affirm
- yes
- indeed
- of course
- that sounds good
- correct

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really

## intent:mood_great
- perfect
- very good
- great
- amazing
- wonderful
- I am feeling very good
- I am great
- I'm good

## intent:mood_unhappy
- sad
- very sad
- unhappy
- bad
- very bad
- awful
- terrible
- not very good
- extremely sad
- so sad

## intent:bot_challenge
- are you a bot?
- are you a human?
- am I talking to a bot?
- am I talking to a human?

## intent:inform
- [Dhaka] (location)
- [Khulna] (location)
- [Uttara] (location)
- [Dhanmondi] (location) 
- [Baridhara] (location)
- [tutpara] (location)

## intent: search_provider
- I need a [hospital] (facility_type)
- Find me a nearby [hospital] (facility_type) in [Khulna] (location)
- [hospital] (facility_type)
- Clinic (facility_type) 
- [hospital] (facility_type) kothay 
- hasapatal (facility_type)  koi
- i need a [home health agency ] (facility_type) 
- nearby [hospital] (facility_type) proyojon 